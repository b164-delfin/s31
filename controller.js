const Task = require('./model');


module.exports.getAllTasks = () => {
	return Task.find({}).then( result => {
		return result;
	})
};


module.exports.createTask = (requestBody) => {


	let newTask = new Task ({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})

}