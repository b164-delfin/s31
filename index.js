const express = require('express');
const mongoose = require('mongoose');

const taskRoute = require('/route');

const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended:true }));


mongoose.connect('mongodb+srv://kyoko:Kyoko1990@cluster0.hxds7.mongodb.net/batch164_to-do?retryWrites=true&w=majority',
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));


app.use("/model", taskRoute);






app.listen(port, () => console.log(`Now listening to port ${port}`));

