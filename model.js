const mongoose = require("mongoose");

const index = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}
})


module.exports = mongoose.model("TASK", index);
